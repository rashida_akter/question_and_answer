public class Answer {
    private int answerId;
    private String answerDetails;

    public Answer(int answerId, String answerDetails){
        this.answerId = answerId;
        this.answerDetails = answerDetails;
    }

    public int getAnswerId(){
        return answerId;
    }
    public void setAnswerId(int answerId){
        this.answerId = answerId;
    }

    public String getAnswerDetails(){
        return answerDetails;
    }
    public void setAnswerDetails(String answerDetails){
        this.answerDetails = answerDetails;
    }
}
