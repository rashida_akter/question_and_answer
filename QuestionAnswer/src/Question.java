import java.util.*;

public class Question {
    private int questionId;
    private String questionDetails;
    Answer A;

    public Question(int questionId, String questionDetails, Answer A){
        this.questionId = questionId;
        this.questionDetails = questionDetails;
        this.A = A;
    }

    public int getQuestionId(){
        return questionId;
    }
    public void setQuestionId(int questionId){
        this.questionId = questionId;
    }

    public String getQuestionDetails(){
        return questionDetails;
    }
    public void setQuestionDetails(String questionDetails){
        this.questionDetails = questionDetails;
    }

    public String getA(){
        return A.getAnswerDetails();
    }
    public void setA(Answer A){
        this.A = A;
    }
}
