import java.util.ArrayList;

public class Main {
    public static void main(String[] args){

        Question[] q = new Question[5];
        q[0] = new Question(1, "Data type of '1'", new Answer(1, "Integer."));
        q[1] = new Question(2, "Data type of 'A'", new Answer(2, "Character."));
        q[2] = new Question(3, "Data type of 'Niha'", new Answer(3, "String."));
        q[3] = new Question(4, "Data type of '0.5'", new Answer(4, "Float."));
        q[4] = new Question(5, "Data type of 'Object'", new Answer(5, "It's a class type variable."));

        for(int i=0; i<5; i++){
            System.out.println(q[i].getQuestionDetails() + ": " + q[i].getA());
        }
    }
}
